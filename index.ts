export class Semaphore {
  private s: number
  private q: (() => void)[] = []

  get size() {
    return this.s
  }

  constructor(initialSize: number) {
    if (
      !(
        (Number.isInteger(initialSize) && initialSize > 0) ||
        initialSize === Infinity
      )
    ) {
      throw new Error("size must be a positive integer or Infinity.")
    }
    this.s = initialSize
  }

  async wait(): Promise<() => void> {
    if (this.s > 0) {
      this.s--
      return once(() => this.release())
    }

    await new Promise<void>(r => {
      this.q.push(r)
    })

    return this.wait()
  }

  private release() {
    this.s++

    if (this.s > 0 && this.q.length > 0) {
      this.q.shift()!()
    }
  }
}

function once(fn: () => void) {
  let called = false
  return function onceWrapped() {
    if (called) {
      throw new Error("This function may only be called once.")
    }
    called = true
    fn()
  }
}
