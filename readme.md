# Semaphore

Simple async semaphore

If you like this package, be sure to star its repo, and please consider [donating](https://seangenabe.netlify.com/donate).

## Usage

```typescript
import { Semaphore } from "@seangenabe/semaphore"

const semaphore = new Semaphore(64)

;(async () => {
  const release = await semaphore.wait()
  // ...
  release()
})()
```


### const semaphore = new Semaphore(initialSize)

Parameters:
* `initialSize` - the maximum number of claims allowed. Must be a positive integer or `Infinity`.

Creates a new semaphore with the specified size.



### semaphore.wait(): Promise<() => void>

Returns: `Promise<() => void>`

Waits for other claims on this semaphore, then resolves with a function that can be called to release the claim on this semaphore.



### semaphore.size: number

Read-only access to the remaining number of claims that can be taken.
